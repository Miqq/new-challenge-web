import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SolucionesComponent } from './components/soluciones/soluciones.component';
import { UserComponent } from './components/user/user.component';
import { WrapperComponent } from './components/wrapper/wrapper.component';
import { BlogComponent } from './components/blog/blog.component';
import { LmsComponent } from './components/lms/lms.component';
import { PostComponent } from './components/post/post.component';
import { CursoComponent } from './components/curso/curso.component';
import { LmsSeccionesComponent } from './components/lms-secciones/lms-secciones.component';
import { LmsRecursosComponent } from './components/lms-recursos/lms-recursos.component';
import { VideoComponent } from './components/video/video.component';


const routes: Routes = [
    {
        path: '',
        component: WrapperComponent,
        children: [
          {
            path: 'dashboard',
            component: DashboardComponent
          },
          {
            path: 'soluciones',
            component: SolucionesComponent
          },
          {
            path: 'dashboard',
            component: DashboardComponent
          },
          {
            path: 'blog',
            component: BlogComponent
          },
          {
            path: 'lms',
            component: LmsComponent
          },
          {
            path: 'post',
            component: PostComponent
          },
          {
            path: 'curso',
            component: CursoComponent
          },
          {
            path: 'lmsSecciones',
            component: LmsSeccionesComponent
          },
          {
            path: 'lmsRecursos',
            component: LmsRecursosComponent
          },
          {
            path: 'video',
            component: VideoComponent
          }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
