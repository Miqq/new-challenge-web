import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { WrapperComponent } from './components/wrapper/wrapper.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { InfoComponent } from './components/info/info.component';
import { UserComponent } from './components/user/user.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatTabsModule} from '@angular/material/tabs';
import {MatCardModule} from '@angular/material/card';
import {MatExpansionModule} from '@angular/material/expansion';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SolucionesComponent } from './components/soluciones/soluciones.component';
import { BlogComponent } from './components/blog/blog.component';
import { LmsComponent } from './components/lms/lms.component';
import { PostComponent } from './components/post/post.component';
import { CursoComponent } from './components/curso/curso.component';
import { LmsSeccionesComponent } from './components/lms-secciones/lms-secciones.component';
import { LmsRecursosComponent } from './components/lms-recursos/lms-recursos.component';
import { VideoComponent } from './components/video/video.component';
import { YouTubePlayerModule } from "@angular/youtube-player";


@NgModule({
  declarations: [WrapperComponent, DashboardComponent, InfoComponent, UserComponent, SolucionesComponent, BlogComponent, LmsComponent, PostComponent, CursoComponent, LmsSeccionesComponent, LmsRecursosComponent, VideoComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    YouTubePlayerModule,

    // NG Material Modules
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatToolbarModule,
    MatButtonModule,
    MatTabsModule,
    MatCardModule,
    MatExpansionModule,
    NgbModule
  ]
})
export class DashboardModule { }
