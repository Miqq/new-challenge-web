import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LmsSeccionesComponent } from './lms-secciones.component';

describe('LmsSeccionesComponent', () => {
  let component: LmsSeccionesComponent;
  let fixture: ComponentFixture<LmsSeccionesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LmsSeccionesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LmsSeccionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
