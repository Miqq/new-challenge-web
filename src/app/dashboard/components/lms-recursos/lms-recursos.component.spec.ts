import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LmsRecursosComponent } from './lms-recursos.component';

describe('LmsRecursosComponent', () => {
  let component: LmsRecursosComponent;
  let fixture: ComponentFixture<LmsRecursosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LmsRecursosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LmsRecursosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
