# NewChallenge

Este proyecto fue generado utilizando [Angular CLI](https://github.com/angular/angular-cli) version 11.0.6.

## Requisitos

1. Node.js
2. Angular
3. Cualquier editor de texto

## Correr el proyecto en servidor local (Development server)

Correr `ng serve` en la terminal para montar la pagina en un servidor local. En su navegador, ingrese a `http://localhost:4200/`. En caso de hacer un cambio en el codigo, la pagina lo actualizara automaticamente al guardar.

## Code scaffolding

Correr `ng generate component ejemplo-componente` para generar un nuevo componente. Tambien se puede utilizar `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Correr `ng build` para hacer un build del proyecto. Los artifacts se guardaran en el directorio `dist/`. Utilize `--prod` para una production build.

## Pruebas unitarias

Correr `ng test` para ejecutar una prueba unitaria utilizando [Karma](https://karma-runner.github.io).

## Pruebas end-to-end

Correr `ng e2e` para ejecutar pruebas end-to-end utilizando [Protractor](http://www.protractortest.org/).

## Mas ayuda

Para encontrar mas ayuda de Angular CLI, utilize `ng help` o utilize la siguiente liga [Angular CLI Overview and Command Reference](https://angular.io/cli).
